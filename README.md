With this application you can easily add dates to the PHPJabbers free availability calendar, and immediately upload it to your website. 
Just make sure to change FilePath to your availability html file and insert your SFTP credentials into the variables.

Link to PHPJabbers' free script: https://www.phpjabbers.com/free-availability-calendar-script/