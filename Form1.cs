﻿using System;
using System.IO;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net;
using Renci.SshNet;
using Renci.SshNet.Sftp;

namespace ZomerhuisjeKalender
{
    public partial class Form1 : Form
    {
        // Replace this information
        // HOST, PORT, USERNAME, PASSWORD
        public List<string> clientCredentials = new List<string> {"HOST HERE", "PORT HERE", "USERNAME HERE", "PASSWORD HERE" };

        // Path on the server to file using PHPJabbers' calendar
        private static string SFTPPath = @"public_html/beschikbaarheid.html";
        // Path on the server to map with file using PHPJabbers' calendar
        private static string SFTPMapPath = @"public_html/";
        // Local Path on your machine to file using PHPJabbers' calendar
        private static string FilePath = @"ZomerhuisjeWebsite2/beschikbaarheid.html";
        // Local Path to backups file
        private static string FilePathBackup = @"backups.txt";

        private List<DateTime> allDates = new List<DateTime>();
        
        private string a = ""; // The already existing dates go into this string. 
        private int lineNumber = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dateTimePicker1.MinDate = (DateTime.Today).Date;
            dateTimePicker2.MinDate = (DateTime.Today.AddDays(1)).Date;
            DownloadFile(new SftpClient(clientCredentials[0], Convert.ToInt32(clientCredentials[1]), clientCredentials[2], clientCredentials[3]), FilePath, SFTPPath); // Download de newest 'beschikbaarheid.html' that's on the site. op de website staat.
            DatesReader();
        }
        // Replace this information
        private void button1_Click(object sender, EventArgs e)
        {
            DateTime aankomst = dateTimePicker1.Value;
            DateTime vertrek = dateTimePicker2.Value;
            if (dateTimePicker1.Value == dateTimePicker2.Value)
            {
                MessageBox.Show("You have selected the same day twice!");
            }
            else if (aankomst < vertrek)
            {
                DatesInBetween(aankomst, vertrek); // Generates the dates that are in between the arrival date and the departure, and adds them into the list allDates.
                writeDates();
                MessageBox.Show("Dates succesfully added!");
            }
            else
            {
                MessageBox.Show("An error has occurred. Is the date of arrival earlier than the date of departure?");
            }
        }
        
        private void button2_Click(object sender, EventArgs e)
        {
            DateTime datum1 = (dateTimePicker4.Value).Date; // Date of arrival (for removal)
            DateTime datum2 = (dateTimePicker3.Value).Date; // Date of departure (for removal)
            if (datum2 > datum1)
            {
                RemoveDatesInBetween(datum1, datum2);
                writeDates();
                MessageBox.Show("Dates succesfully removed!");
            }
            else
            {
                MessageBox.Show("An error has occurred. Is the first date earlier than the second date?");
            }
        }
        private void writeDates() { // Writes the list allDates to the file.
            if (File.Exists(FilePath))
            {
                string stringToWrite = "unavailable: ['";
                var bestand = File.ReadAllLines(FilePath); // Save FilePath in the memory
                if (allDates.Any())
                {
                    foreach (DateTime x in allDates)
                    {
                        if (x == allDates[(allDates.Count) - 1])
                        {
                            stringToWrite = stringToWrite + x.ToString("yyyy/MM/dd") + "']";
                        }
                        else
                        {
                            stringToWrite = stringToWrite + x.ToString("yyyy/MM/dd") + "', '";
                        }
                    }
                    stringToWrite = stringToWrite.Replace("/", "-");
                }
                bestand[lineNumber - 1] = stringToWrite; // Replace the current unavailable line in the memory with the new unavailable string
                File.AppendAllText(FilePathBackup, DateTime.Now.ToString() + " - " + stringToWrite + Environment.NewLine);
                File.WriteAllLines(FilePath, bestand); // Write the memory to the file
                UploadSFTPFile(new SftpClient(clientCredentials[0], Convert.ToInt32(clientCredentials[1]), clientCredentials[2], clientCredentials[3]), FilePath, SFTPMapPath);
            }
            else
            {
                MessageBox.Show("Error: Local website files not found!");
            }
        }

        private void DatesInBetween(DateTime startingDate, DateTime endingDate)
        {
            for (DateTime date = startingDate; date <= endingDate; date = date.AddDays(1))
            {
                date = date.Date; // Convert the dates to always be at 12AM
                if (!allDates.Contains(date))
                {
                    allDates.Add(date);
                }
            }
        }

        private void RemoveDatesInBetween(DateTime startingDate, DateTime endingDate) // Remove all dates between the startingDate and the endingDate.
        {
            startingDate = startingDate.Date;
            endingDate = endingDate.Date;
            for (DateTime date_ = startingDate; date_ <= endingDate; date_ = date_.AddDays(1))
            {
                allDates.Remove(date_);
            }
        }

        private void DatesReader() { // Executed on startup. Searches for the unavailable line in the file, and adds the dates to the allDates list.
            if (File.Exists(FilePath)) {
                foreach (string line in File.ReadLines(FilePath))
                {
                    lineNumber++;
                    if (line.Contains("unavailable: [")) // When the unavailable line has been found, save it in the 'a' variable
                    {
                        a = line;

                        break;
                    }
                }
                if (a != "unavailable: ['")
                {
                    a = a.Remove(0, 15); // remove "unavailable: ["
                    a = a.Remove(a.Length - 2, 2); // remove "]"
                    a = a.Replace("'", string.Empty);
                    a = a.Replace(",", string.Empty);
                    a = a.Replace("-", "/");
                    List<string> readDates = a.Split(' ').ToList(); // Read the individual dates and add them into this readDates list
                    foreach (string date in readDates)
                    {
                        DateTime X = (DateTime.ParseExact(date, "yyyy/MM/dd", System.Globalization.CultureInfo.InvariantCulture)).Date;
                        allDates.Add(X);
                    } // Convert the dates to the DateTime type and add them to the allDates list.
                }
            }
        }

        public void UploadSFTPFile(SftpClient sftpclient, string sourcefile, string destinationpath) // Uploads a file (sourcefile) to the SFTP server, in the destinationpath.
        {
            using (sftpclient)
            {
                sftpclient.Connect();
                sftpclient.ChangeDirectory(destinationpath);
                using (FileStream fs = new FileStream(sourcefile, FileMode.Open))
                {
                    sftpclient.BufferSize = 4 * 1024;
                    sftpclient.UploadFile(fs, Path.GetFileName(sourcefile));
                }
                sftpclient.Disconnect();
            }
        }

        private void DownloadFile(SftpClient sftpclient, string pathToDownloadTo, string pathToDownloadFrom) // Executed on startup. Downloads the latest HTML file from your website. 
        {
            using (sftpclient)
            {
                try
                {
                    sftpclient.Connect();
                    using (Stream fileStream = File.OpenWrite(pathToDownloadTo))
                    {
                        sftpclient.DownloadFile(pathToDownloadFrom, fileStream);
                    }
                    sftpclient.Disconnect();
                }
                catch (Exception er)
                {
                    MessageBox.Show("Error: latest version of the calendar hasn't been downloaded. Log:\n " + er);
                }
            }
        }
    }
}
